# Storage 

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/js-storage/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/js-storage/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/cherrypulp/js-storage/badges/build.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/js-storage/build-status/master)
[![codebeat badge](https://codebeat.co/badges/e140800a-4cfb-414d-a575-62652b030f54)](https://codebeat.co/projects/github-com-cherrypulp-js-storage-master)
[![GitHub issues](https://img.shields.io/github/issues/cherrypulp/js-storage)](https://github.com/cherrypulp/js-storage/issues)
[![GitHub license](https://img.shields.io/github/license/cherrypulp/js-storage)](https://github.com/cherrypulp/js-storage/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Fstorage.svg)](https://badge.fury.io/js/%40cherrypulp%2Fstorage)

Basic key/value storage functionality with plugins support.


## Installation

`npm install @cherrypulp/storage`


## Quick start

```javascript
import Storage from '@cherrypulp/storage';

```

TODO



## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://github.com/monkeymonk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://github.com/cherrypulp/js-storage/blob/master/LICENSE) file for details.



## TODOS

- [ ] Documentation
- [ ] Unit tests
